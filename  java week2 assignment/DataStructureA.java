package com.greatLearning.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class DataStructureA {
	
	public void sortingNames(ArrayList<Employee> employees) {
		Collections.sort(employees, new sortByName());
		ArrayList<String> names = new ArrayList<String>();
		Iterator<Employee> itr = employees.iterator();
		
		while(itr.hasNext()) {
			Employee e = itr.next();
			names.add(e.getName());
		}
		
		System.out.println(names);
	}

}
