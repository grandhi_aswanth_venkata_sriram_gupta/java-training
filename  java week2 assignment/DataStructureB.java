package com.greatLearning.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;



public class DataStructureB {
	//Ex 3
	public void cityNameCount(ArrayList<Employee> employees) {
	
		Set<String> cities = new HashSet<String>();
		Iterator<Employee> itr = employees.iterator();
		while(itr.hasNext()) {
			Employee e = itr.next();
			cities.add(e.getCity());
		}
		Iterator<String> itr1 = cities.iterator();
		TreeMap<String, Integer> cityCount = new TreeMap<String, Integer>();
		while(itr1.hasNext()) {
			String city = itr1.next();
			cityCount.put(city, Collections.frequency(employees, new Employee(city)));
		}
		System.out.println(cityCount);	
	}
	
	//Ex 4
	
	public void monthlySalary(ArrayList<Employee> employees) {
		
		HashMap<Integer,Double> monSal = new HashMap<Integer, Double>();
		Iterator<Employee> itr = employees.iterator();
		while(itr.hasNext()) {
			Employee e = itr.next();
			try {
				monSal.put(e.getId(), (double) (e.getSalary()/12));
			} catch (Exception e2) {
				System.out.println(e2);
			}
			
			
		}
		System.out.println(monSal);
	}

}
