package com.greatLearning.assignment2;

public class Employee implements Comparable<Employee>{
	private int id;
	private String name;
	private int age;
	private int salary; // per annum
	private String department;
	private String city;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", department="
				+ department + ", city=" + city + "]\n";
		
		//return "Employee ID : "+id+"\nEmployee Name : "+name+ "\nEmployeeAge : "+age+
				//"\nEmployee Salary : "+salary+"\nEmployee City : "+city+"\nEmployee Department : "+department+"\n";
	}
	@Override
	public int compareTo(Employee e) {
		// TODO Auto-generated method stub
		return this.name.compareTo(e.name);
	}
	

	
	public Employee(int id, String name, int age, int salary, String department, String city) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.department = department;
		this.city = city;
	}
	
	
	
	public Employee(String city) {
		super();
		this.city = city;
	}
	@Override
	public boolean equals(Object o) {
		Employee e;
		if(!(o instanceof Employee))
        {
            return false;
        }
		else {
			e=(Employee)o;
			if(this.city.equals(e.getCity()))
				return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	


}
