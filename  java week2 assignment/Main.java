package com.greatLearning.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Employee> employees = new ArrayList<Employee>();
		
		Employee emp1 = new Employee(1, "Aman", 20, 1100000, "IT", "Delhi" );
		Employee emp2 = new Employee(2, "Bobby", 22, 500000, "HR", "Bombay" );
		Employee emp3 = new Employee(3, "Zoe", 20, 750000, "Admin", "Delhi" );
		Employee emp4 = new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai");
		Employee emp5 = new Employee(5, "Smitha", 24, 1200000, "HR", "Bengaluru");
		employees.add(emp1);
		employees.add(emp2);
		employees.add(emp3);
		employees.add(emp4);
		employees.add(emp5);
		
		//ex 1
		//Collections.sort(employees);
		System.out.println("\n\n1-Print All Employee Details\n");
		Iterator<Employee> itr = employees.iterator();
		while(itr.hasNext()) {
			Employee e = itr.next();
			System.out.println("Employee ID : "+e.getId()+"\nEmployee Name : "+e.getName()+ "\nEmployeeAge : "+e.getAge()+
					"\nEmployee Salary : "+e.getSalary()+"\nEmployee City : "+e.getCity()+"\nEmployee Department : "+e.getDepartment()+"\n");
		}
		
		//System.out.println(employees);
		
		//ex 2
		System.out.println("\n\n2-Sort By Emp Name\n");
		DataStructureA d1 =new DataStructureA(); 
		d1.sortingNames(employees);
		
		//ex3
		System.out.println("\n\n3-City Count\n");
		DataStructureB d3 =new DataStructureB(); 
		d3.cityNameCount(employees);
		
		
		
		//ex 4
		System.out.println("\n\n4-Id with Month Salary\n");
		d3.monthlySalary(employees);
		
		
		

	}

}
